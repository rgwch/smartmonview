import { exec } from 'child_process'
import * as os from 'os'
import { version, name, description } from '../package.json'

export class Controller {
  private sy = "linux"
  constructor() {
    if (os.platform() == 'darwin') {
      this.sy = "mac"
    } else if (os.platform() == 'linux') {
      this.sy = "linux"
    }
  }

  /**
   * Fetch general status informations, Check if smartmontools are present
   */
  public async status(): Promise<any> {
    const ret = {
      name, version, description, platform: this.sy, smartctl: ""
    }
    try {
      const smartctl = await this.launch("smartctl --version")
      if (smartctl) {
        const cpr = smartctl.split(/\n/)
        ret.smartctl = cpr[0] + " " + cpr[1]
        return ret
      }
    } catch (err) {
      ret.smartctl = "** ERROR ** " + err
      return ret;
    }

  }

  /**
   * List all available drives
   */
  public async listDrives(): Promise<any> {
    //console.log(this.sy)
    if (this.sy == "mac") {
      const disks = await this.launch("diskutil list")
      const re = /^(\/dev\/[^:\n]+)/mg
      // console.log(disks)
      const m = disks.match(re)?.map(el => {
        const d = el.split(/\s+/)
        return { handle: d[0], description: d[0] + " " + d[1] }
      })
      return { title: "macOS drives ", drives: m }

    } else if (this.sy == "linux") {
      const disks = await this.launch("smartctl --scan")
      const re = /^(\/dev\/[^:\n]+)/mg
      // console.log(disks)
      const m = disks.match(re)?.map(el => {
        const d = el.split(/\s+#\s+/)
        const h = d[0].split(/\s+/)
        return { handle: h[0], description: d[1] }
      })
      return { title: "Linux drives ", drives: m }
    }
    else {
      throw new Error("undefined system " + this.sy)
    }
  }

  /**
   * Get SMART informations on a drive
   * @param disk 
   */
  public async info(disk: string) {
    const i = await this.launch("smartctl -a " + disk)
    return {
      title: "Drive " + disk,
      message: i,
      disk
    }
  }

  /**
   * Run a SMART test (background).
   * @param disk: drive to test 
   * @param mode: "short" (will take minuts) or "long" (will take hours)
   */
  public async test(disk: string, mode: string) {
    const ans = await this.launch(`smartctl -t ${mode} ${disk}`)
    return {
      title: `Drive: ${disk}, test: ${mode} launched`,
      message: ans,
      disk: disk
    }
  }

  private launch(prog: string): Promise<string> {
    return new Promise((resolve, reject) => {
      exec(prog, (error, stdout, stderr) => {
        if (stdout) {
          resolve(stdout)
        }
        if (stderr) {
          reject(stderr)
        }
        if (error) {
          reject(error)
        }
      })
    })

  }

}