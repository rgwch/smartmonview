import { Controller } from './controller';
import chai from 'chai'
chai.should()

describe("controller", () => {
  const c = new Controller()
  it("returns version information", async () => {
    const v = await c.status()
    v.should.be.ok
    v.should.have.property("name")
    v.name.should.be.ok
    v.should.have.property("version")
    v.version.should.match(/\d+\.\d+\.\d+/)
    v.should.have.property("description")
    v.description.should.be.ok
    v.should.have.property("platform")
    v.should.have.property("smartctl")
    v.smartctl.startsWith("**").should.be.false
  })

  it('fetches disk informations', async()=>{
    const di=await c.listDrives()
    di.should.be.ok
    di.should.have.property("title")
    di.should.have.property("drives")
    di.drives.should.be.an("Array")
    
  })

  it('fetches smart status of first disk',async ()=>{
    const di=await c.listDrives()
    const first=di.drives[0]
    first.should.have.property("handle")
    first.should.have.property("description")
    const data=c.info(first.handle)
    data.should.be.ok
  })
})