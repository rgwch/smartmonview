import { Controller } from './lib/controller';
import express = require('express')
import path from 'path'

export const app = express.application = express()
app.use(express.urlencoded({ extended: true }))
app.set("view engine", "pug")
app.use(express.static(path.join(__dirname, "../static")))

const ctl = new Controller()

app.get("/", async (req, res) => {
  try {
    const status = await ctl.status()
    if (status.smartctl.startsWith("**")) {
      res.render("error", { msg: "Cant't launch smartclt. Smartmontools installed?" })

    } else {
      const disks = await ctl.listDrives()
      res.render("main", disks)
    }
  } catch (err) {
    res.render("error", { msg: err })
  }
})

app.get("/fetch", async (req, res) => {
  try {
    const drive = req.query.drive as string
    const d = drive.split(/\s+/)[0]
    const r = await ctl.info(d)
    res.render("info", r)
  } catch (err) {
    res.render("error", { msg: err })
  }
})

app.get("/test", async (req, res) => {
  try {
    const ans = await ctl.test(req.query.disk as string, req.query.mode as string)
    res.render("info", ans)
  } catch (err) {
    res.render("error", { msg: err })
  }
})
app.listen(5281, () => {
  console.log("running on port 5281")
})

