import chai from 'chai'
import chaiHttp from 'chai-http'
import 'mocha'
import {app} from './index'
chai.use(chaiHttp)
chai.should()
const expect=chai.expect

describe('smartmonview', () => {
  it('does something', async () => {
    const res=chai.request(app).get("/")
    res.should.equal("Hallo")    
  })
})