## What is it?

A very simple web frontend for the [Smartmontools](https://www.smartmontools.org/). it doesn't give much more than `smartctl -a/-t`, but it allows to do it in a browser.

## Prerequisites

* Linux or macOS computer
* smartmontools installed (e.g. `sudo apt install smartmontools` or `brew install smartmontools`)
* NodeJS 10.x or better installed

## Install

### Just for trying:
```
git clone https://gitlab.com/rgwch/smartmonview
cd smartmonview
npm i
sudo npm start
```
(sudo is required on many systems, since access to S.M.A.R.T functions is restricted)

Navigate the browser to http://servername:5281. It should open with a list of available drives to chose from.

### As a service
Recommended, if you want to access smartmon more frequently

```
npm run build
sudo npm i -g forever
sudo npm i -g forever-service
sudo forever-service install smartmonview --script build/index.js
sudo service smartmonview start
```
That way, smartmonview will run in background, and is automatically restarted on reboot. To stop it, use `sudo service smartmonview stop`. To remove it completely, use `sudo forever-service delete smartmonview`.

## Security advice

Smartmontools runs with root access, and smartmonview gives access to some functions of smartmontools without requiring authentication. So DON'T expose smartmonview's port to the internet and use it only behind a firewall and only within a trusted network where only trustworthy people have access.
